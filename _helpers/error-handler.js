const rabbitmq = require('_helpers/rabbitmq');

function errorHandler(err, req, res, next) {
    if (typeof (err) === 'string') {
        // custom application error
        rabbitmq.sendToMQ('users.error', { status: 400, error: err });
    }

    if (err.name === 'ValidationError') {
        // mongoose validation error
        rabbitmq.sendToMQ('users.error', { status: 400, error: err });
    }

    if (err.name === 'UnauthorizedError') {
        // jwt authentication error
        rabbitmq.sendToMQ('users.error', { status: 401, error: err });
    }

    // default to 500 server error
    rabbitmq.sendToMQ('users.error', { status: 500, error: err });
}

module.exports = errorHandler;