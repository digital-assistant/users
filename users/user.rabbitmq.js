const rabbitmq = require('_helpers/rabbitmq');
const UserService = require('./user.service');
const config = require('config');
const jwt = require('jsonwebtoken');

rabbitmq.getFromMQ('front', 'user.create', msg => {
    let user = JSON.parse(msg.content.toString());
    console.log("[user] %s",'user.create');
    UserService.create(user)
    .then(() => {
        rabbitmq.sendToMQ('user.created', user);
    })
    .catch(err => {
        rabbitmq.sendToMQ('user.create.error', {err: err});
        throw err;
    });
});

rabbitmq.getFromMQ('front', 'user.authenticate', msg => {
    let user = JSON.parse(msg.content.toString());
    console.log("[user] %s",'user.authenticate');
    UserService.authenticate(user)
    .then(user => {
        if (user) {
            console.log("[user] %s",'user.authenticated');
            rabbitmq.sendToMQ('user.authenticated', user);
        } else {
            console.log("[user] %s",'user.authenticate.error');
            rabbitmq.sendToMQ('user.authenticate.error', {err: 'Check your credentials!'});
        }
    })
    .catch(err => {
        rabbitmq.sendToMQ('user.authenticate.error', {err: err});
        throw err;
    });
});

rabbitmq.getFromMQ('front', 'user.get.by.auth-token', msg => {
    let params = JSON.parse(msg.content.toString());
    console.log("[user] %s",'user.get.by.auth-token');
    let decoded = jwt.verify(params.token, config.secret);
    UserService.getById(decoded.sub)
    .then(user => {
        if (user) {
            rabbitmq.sendToMQ('user.got.by.auth-token', user);
        } else {
            rabbitmq.sendToMQ('user.get.by.auth-token.error', {err: 'something went wrong!'});
        }
    })
    .catch(err => {
        rabbitmq.sendToMQ('user.get.by.auth-token.error', {err: err});
        throw err;
    });
});

rabbitmq.getFromMQ('front', 'user.update', msg => {
    let params = JSON.parse(msg.content.toString());
    console.log("[user] %s",'user.update');
    let decoded = jwt.verify(params.token, config.secret);

    UserService.update(decoded.sub, params.form)
    .then(() => {
        UserService.getById(decoded.sub)
            .then(user => {
                if (!user) {
                    return rabbitmq.sendToMQ('user.update.error', {err: 'Not Found!'});
                }
                rabbitmq.sendToMQ('user.updated', user);
            })
            .catch(err => {
                rabbitmq.sendToMQ('user.update.error', {err: err});
                throw err;
            });
    })
    .catch(err => {
        rabbitmq.sendToMQ('user.update.error', {err: err});
        throw err;
    });
});

rabbitmq.getFromMQ('front', 'user.get.all', msg => {
    console.log("[user] %s",'user.get.all');
    UserService.getAll()
    .then(users => {
        if (users) {
            rabbitmq.sendToMQ('user.got.all', users);
        } else {
            rabbitmq.sendToMQ('user.get.all.error', {err: 'something went wrong!'});
        }
    })
    .catch(err => {
        rabbitmq.sendToMQ('user.get.all.error', {err: err});
        throw err;
    });
});

rabbitmq.getFromMQ('front', 'user.get.by.params', msg => {
    let params = JSON.parse(msg.content.toString());
    console.log("[user] %s",'user.get.by.params');
    UserService.getAll(params)
    .then(users => {
        if (users) {
            rabbitmq.sendToMQ('user.got.by.params', users);
        } else {
            rabbitmq.sendToMQ('user.get.by.params.error', {err: 'something went wrong!'});
        }
    })
    .catch(err => {
        rabbitmq.sendToMQ('user.get.by.params.error', {err: err});
        throw err;
    });
});

rabbitmq.getFromMQ('front', 'user.get.by.id', msg => {
    let params = JSON.parse(msg.content.toString());
    console.log("[user] %s",'user.get.by.id');
    UserService.getById(params.id)
    .then(user => {
        if (user) {
            rabbitmq.sendToMQ('user.got.by.id', user);
        } else {
            rabbitmq.sendToMQ('user.get.by.id.error', {err: 'something went wrong!'});
        }
    })
    .catch(err => {
        rabbitmq.sendToMQ('user.get.by.id.error', {err: err});
        throw err;
    });
});

rabbitmq.getFromMQ('front', 'user.delete', msg => {
    let params = JSON.parse(msg.content.toString());
    console.log("[user] %s",'user.delete');
    let decoded = jwt.verify(params.token, config.secret);
    UserService.delete(decoded.sub)
        .then(() => {
            rabbitmq.sendToMQ('user.deleted', {});
        })
        .catch(err => {
            rabbitmq.sendToMQ('user.delete.error', {err: err});
            throw err;
        });
});

rabbitmq.getFromMQ('front', 'user.get.avail.day', async msg => {
    let params = JSON.parse(msg.content.toString());
    console.log("[user] %s",'user.get.avail.day');
    UserService.getByIdAndAvail(params.id, params.day)
    .then(user => {
        if (user) {
            rabbitmq.sendToMQ('user.got.avail.day', user);
        } else {
            rabbitmq.sendToMQ('user.get.avail.day.error', {err: 'something went wrong!'});
        }
    })
    .catch(err => {
        rabbitmq.sendToMQ('user.get.avail.day.error', {err: err});
        throw err;
    });
});
