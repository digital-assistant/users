const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const schema = new Schema({
    username: { type: String, index: true, unique: true, required: true },
    name: { type: String, required: true },
    hash: { type: String, required: true },
    details: Schema.Types.Mixed,
}, {
    timestamps: true
});

schema.set('toJSON', { virtuals: true });

module.exports = mongoose.model('User', schema);
