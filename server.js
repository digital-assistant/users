require('rootpath')();
const express = require('express');
const app = express();
const errorHandler = require('_helpers/error-handler');

require('./users/user.rabbitmq');

// global error handler
app.use(errorHandler);

// start server
const port = process.env.PORT ? process.env.PORT : 3000;
if (!module.parent) {
    app.listen(port, function () {
        console.log('Server listening on port ' + port);
    });
}
module.exports = app;
